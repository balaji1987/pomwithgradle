package selfwork;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDownMercury {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/newtours/register.php");
		driver.manage().window().maximize();
		Select dropCountry = new Select(driver.findElementByName("country"));
		dropCountry.selectByVisibleText("INDIA");
		//dropCountry.selectByIndex(10);
		//dropCountry.selectByValue("AUSTRALIA");
		if (dropCountry.isMultiple()) {
			System.out.println("Dropdown is Multiple");
		}
			else {
				System.out.println("Dropdown is not mutiple");
		}
		//dropCountry.deselectByVisibleText("INDIA");
	}

}
