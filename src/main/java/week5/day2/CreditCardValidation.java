package week5.day2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreditCardValidation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		String creditCard = "1234 5467 8901 2345";
//		String pat = "[0-9]{4}\\s[0-9]{4}\\s[0-9]{4}\\s[0-9]{4}";
//		Pattern compile = Pattern.compile(pat);
//		Matcher matcher = compile.matcher(creditCard);
//		System.out.println(matcher.matches());
		
		String input = "axdxcdgxbxx";
		String x = "";
		char[] charArray = input.toCharArray();
		for (char c: charArray) {
			if (c == 'x') {
				x+=c;
			}
		}
		String replace = input.replace("x", "");
		String concat = replace.concat(x);
		System.out.println(concat);
	}

}
