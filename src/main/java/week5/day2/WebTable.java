package week5.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.leafground.com/pages/table.html");
		driver.manage().window().maximize();
		WebElement webtable = driver.findElementByTagName("table");
		List<WebElement> list = webtable.findElements(By.tagName("tr"));
		System.out.println(list.size());

		for (int i = 1; i<list.size(); i++) {
			List<WebElement> eachCol = list.get(i).findElements(By.tagName("td"));
			System.out.println(eachCol.get(1).getText());
			String progress = eachCol.get(1).getText();
			if(progress.equals("80%"))
				eachCol.get(2).click();
			break;
		}

	}
}
