package week4.day1;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class GenerateReport {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/sample.html");
	html.setAppendExisting(true);
	ExtentReports report = new ExtentReports();
	report.attachReporter(html);
	ExtentTest test = report.createTest("TC001_Login", "Login into the application");
	test.assignAuthor("Balaji");
	test.assignCategory("Regression");
	test.pass("Entered username successfully");
	test.pass("Entered password successfully");
	test.fail("Login successfull");
	report.flush();
	}

}
