package week3.day3;

import org.testng.annotations.DataProvider;

public class GetData {

	@DataProvider (name = "fetchData")
	public String [] [] dynamicData() {
		
		String [][] data = new String [2][3];
		data[0][0] = "CTS";
		data[0][1] = "Balaji";
		data[0][2] = "B";
		
		data[1][0] = "CTS";
		data[1][1] = "Krish";
		data[1][2] = "B";
		
		
	}
}
