package week3.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class TC03_DeleteLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Phone").click();
		driver.findElementByXPath("(//input[contains(@id, 'ext-gen')])[36]").sendKeys("9962813080");
		driver.findElementByXPath("(//button[contains(@id, 'ext-gen')])[7]").click();
		Thread.sleep(3000);
		String rowTable = driver.findElementByXPath("//table[@class='x-grid3-row-table']/tbody/tr/td[1]").getText();
		System.out.println(rowTable);
		driver.findElementByLinkText(rowTable).click();
		driver.findElementByLinkText("Delete").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[contains(@id, 'ext-gen')])[30]").sendKeys(rowTable);
		driver.findElementByXPath("(//button[contains(@id, 'ext-gen')])[7]").click();
		Thread.sleep(3000);
		String errLog = driver.findElementByClassName("x-paging-info").getText();
		System.out.println(errLog);
		String actualErrLog = "No records to display";
		if (errLog.equals(actualErrLog))
			System.out.println("Error Log Verified");
		else
			System.out.println("Mismatch Occured");
		driver.close();
		
	}

}
