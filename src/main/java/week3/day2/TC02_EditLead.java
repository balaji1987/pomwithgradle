package week3.day2;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC02_EditLead extends ProjectSpecificMethod{

	@Test
	public void edit() throws InterruptedException {


		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[contains(@id, 'ext-gen')])[31]").sendKeys("BalajiB");
		driver.findElementByXPath("(//button[contains(@id, 'ext-gen')])[7]").click();
		Thread.sleep(3000);
		String text = driver.findElementByXPath("//table[@class='x-grid3-row-table']/tbody/tr/td[1]").getText();
		System.out.println(text);
		driver.findElementByLinkText(text).click();
		String pageTitle = driver.getTitle();
		//System.out.println(pageTitle);
		String actualTitle = "View Lead | opentaps CRM";
		if(pageTitle.equals(actualTitle))
			System.out.println("Title Matched");
		else
			System.out.println("Title Not Matched");
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Cognizant");
		driver.findElementByClassName("smallSubmit").click();
		String changName = driver.findElementById("viewLead_companyName_sp").getText();
		//
		System.out.println(changName);
		if(changName.contains("Cognizant"))
			System.out.println("Values Updated Correctly");
		else
			System.out.println("Values not updated Correctly");
		driver.close();
	}

}
