package week1.day4;

import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserVerification {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		//getCurrentURL
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		//getTitle
		String title = driver.getTitle();
		System.out.println(title); 	
		//getText

		String textdet = driver.findElementByClassName("requiredField").getText();
		System.out.println(textdet);
		
	}

}
