package week1.homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Email").click();
		driver.findElementByXPath("(//input[contains(@id, 'ext-gen')])[34]").sendKeys("bala.forever87@gmail.com");
		driver.findElementByXPath("(//button[contains(@id, 'ext-gen')])[7]").click();
		Thread.sleep(3000);
		String rowTable = driver.findElementByXPath("//table[@class='x-grid3-row-table']/tbody/tr/td[1]").getText();
		//System.out.println(rowTable);
		driver.findElementByLinkText(rowTable).click();
		driver.findElementByLinkText("Duplicate Lead").click();
		String title = driver.getTitle();
		String actualTitle = "Duplicate Lead | opentaps CRM";
		driver.findElementByLinkText("Create Lead").click();
		if(title.contains("Duplicate Lead"))
			System.out.println("Title Verified");
		else
			System.out.println("Title Mismatch");
		
		
	}

}
