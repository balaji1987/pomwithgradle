
package week1.day2;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateLead {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
		driver.findElementById("createLeadForm_firstName").sendKeys("BalajiB");
		driver.findElementById("createLeadForm_lastName").sendKeys("B");
		driver.findElementById("createLeadForm_dataSourceId").sendKeys("Conference");
		driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Automobile");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("BalajiB");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Balasubramaniyan");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Test");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Selenium");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("10000");
		driver.findElementById("createLeadForm_industryEnumId").sendKeys("Finance");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("50");
		driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Partnership");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123456");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Yes");
		driver.findElementById("createLeadForm_description").sendKeys("Test");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Test");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("600091");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9962813080");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("432324");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Bala");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("bala.forever87@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("Test URL");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Krish");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Shanthi");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Address1");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Address2");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Alabama");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("123456");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("123");
		driver.findElementByClassName("smallSubmit").click();
		//String text = driver.findElementById("viewLead_firstName_sp").getText();
		//System.out.println(text);
		//if (text.contains("BALAJI")) {
		//		System.out.println("Correct value is printed");
		//}
		//else {
		//	System.out.println("The value printed is wrong");
		//}
		
								
	}

}
