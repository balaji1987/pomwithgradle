package week1.day1;

public class MobilePhone {
	
	public String name = "Krish,OnePluss,3T";
	public boolean isMsgSent = true;
	//public short MobModel = 78; 
	
	public String dialcaller() {
		return "krish";
	}
	
	public boolean sendSMS() {
		return true;
	}
	
	public String brandName() {
		return "OnePluss";
	}
	
	public String MobModel() {
		return "3T";
	}
	
    public static void main(String[] args) {
	MobilePhone mobile = new MobilePhone();
	String dialcaller = mobile.dialcaller();
	System.out.println(dialcaller);
	boolean msgsent = mobile.sendSMS();
	System.out.println(msgsent);
	String brname = mobile.brandName();
	System.out.println(brname);
	String modname = mobile.MobModel();
	System.out.println(modname);

    }
}
