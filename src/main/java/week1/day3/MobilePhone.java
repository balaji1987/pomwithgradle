package week1.day3;

public interface MobilePhone {

	public String dialCaller();
	
	public String sendSMS();
	
	//public String playMovies();	
}
