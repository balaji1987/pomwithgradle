package week1.day3;

public class Jio  implements MobilePhone, Telephone{

	@Override
	public String ringCaller() {
		System.out.println("Yes");
		return ("Yes");
	}

	@Override
	public String dialCaller() {
		System.out.println("Praveen");
		return ("Praveen");
	}

	@Override
	public String sendSMS() {
		System.out.println("Msg Sent");
		return ("Msg Sent");
	}

}
