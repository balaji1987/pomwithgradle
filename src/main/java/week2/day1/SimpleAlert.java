package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class SimpleAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementById("loginbutton").click();
		String textMsg = driver.switchTo().alert().getText();
		System.out.println(textMsg);
		driver.switchTo().alert().accept();
		driver.findElementById("usernameId").sendKeys("Balaji");
		//driver.close();
		

	}

}
