package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<String> list = new ArrayList<String>();
		list.add("balaji");
		list.add("Krish");
		list.add("Bala");
		System.out.println(list.size());
		System.out.println(list.get(0));
		System.out.println(list.contains("krish"));
		list.add("balasubramaniyan");
		list.add("Selenium");
		System.out.println(list.size());
		System.out.println(list.remove(4));
		System.out.println("");
		for (String each : list) {
			//System.out.println(each);
		if(each.startsWith("B"))
			System.out.println(each);
		}
	}

}
