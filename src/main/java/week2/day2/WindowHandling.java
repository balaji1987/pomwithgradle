package week2.day2;

import java.awt.List;
import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWindow = driver.getWindowHandles();
		
		ArrayList<String> list = new ArrayList<String>();
		list.addAll(allWindow);
		driver.switchTo().window(list.get(1));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		driver.manage().window().maximize();
		driver.switchTo().window(list.get(0)).close();
	}
}
