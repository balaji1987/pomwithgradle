package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

public class SeMethods implements WdMethods {

	public RemoteWebDriver driver = null;
	int i=1; 
	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			System.out.println("The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue); 
			case "class": return  driver.findElementByClassName(locValue);
			case "linktext" :return driver.findElementByLinkText(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "name": return driver.findElementByName(locValue);
			case "tagname": return driver.findElementByTagName(locValue);
			case "partiallinktext": return driver.findElementByPartialLinkText(locValue);
			case "css": return driver.findElementByCssSelector(locValue);
			default: return null;
			}
		} catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		} 
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);	
			System.out.println("The data "+data+" entered successfully");
		}  catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The element "+ele+ "click successfully");
		}  catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			return ele.getText();
		} catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(value);
			System.out.println("The value "+value+" entered successfully");
		} catch (UnexpectedTagNameException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);
			System.out.println("The value entered successfully");
		}catch(ArrayIndexOutOfBoundsException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch (UnexpectedTagNameException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}		
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
			String actualTitle = driver.getTitle();
			if(actualTitle.equals(expectedTitle))
				return true;
			else
				return false;
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String actualText = ele.getText();
			if(actualText.equals(expectedText))
				System.out.println("Text Verified");
			else
				System.out.println("Texts do not match");
		}catch(NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String actualText = ele.getText();
			if(actualText.contains(expectedText))
				System.out.println("Partial Text verified");
			else
				System.out.println("Texts do not match");
		}catch(NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String actualAttributeValue = ele.getAttribute(attribute);
			//		Assert.assertEquals(actualAttributeValue, value, "The attribute value doesn't match");
			if(actualAttributeValue.equals(value))
				System.out.println("The attribute value matches");
			else
				System.out.println("The attribute value doesn't match");
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String actualAttributeValue=ele.getAttribute(attribute);
			if(actualAttributeValue.contains(value))
				System.out.println("The attribute value matches");
			else
				System.out.println("The attribute value doesn't match");
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected())
				System.out.println("The element is selected");
			else
				System.out.println("The element is not selected");
		} catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {

		try {
			if(ele.isDisplayed())
				System.out.println("The element is displayed");
			else
				System.out.println("The element is not displayed");
		} catch (NoSuchElementException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> window = new ArrayList<String>();
			window.addAll(allWindows);
			driver.switchTo().window(window.get(index));
			window.clear();
		} catch (NoSuchWindowException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
		} catch (NoSuchFrameException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch(WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
		} catch (NoAlertPresentException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch(WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public String getAlertText() {
		try {
			return driver.switchTo().alert().getText();
		} catch (NoAlertPresentException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}catch(WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void takeSnap(){
		try {
			File screenShot = driver.getScreenshotAs(OutputType.FILE); 
			File destination=  new File("./snaps/img"+i+".jpeg");
			FileUtils.copyFile(screenShot, destination);
			i++;
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

	@Override
	public void closeAllBrowsers() {
		try{
			driver.quit();
		}catch(WebDriverException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException();
		}
	}

}
